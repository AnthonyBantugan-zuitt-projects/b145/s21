// ES6 Updates
// BEFORE
      // Math.pow() -> return tyhe base to the exponent power, as in base^exponent
      console.log(Math.pow(7,3));
// NOW
      console.log(7 ** 3);



// Template Literals (``) backticks
      // placeholder => ${}
let from  = 'sir Marts';

let message = 'Hello Batch 145 love ' + from;
console.log(message);

// how to create multi-line strings?
console.log('Hi sirs and mam \n Hello sirs and mam');

console.log(`Hello Batch 145 love, ${from}`);
console.log(`
      Hello
      hello
      hello
`)

// with embedded JS expressions
const interestRate = 1;
const principal = 1000;
console.log(`The interest on your savings account is: ${principal * interestRate}`);
// ============================

let name = 'John';
// before
let message2 = 'Hello ' + name + 'Welcome to programming!';
console.log('message without template literals: ' + message2);

// using template literals
message2 = 

// ===========================================================
// DESTRUCTURE ARRAYS
      // base on the position of the elements
console.warn('Array Destructuring');
let grades = [89,87,78,96];
let fullName = ['Juan', 'Dela', 'Cruz'];

// Pre-Array Destructuring
console.log(fullName[0]); //Juan
console.log(fullName[1]); //Dela

// Array Destructuring
let [firstName, middleName, lastName] = fullName;
console.log(lastName); //Cruz
console.log(`Hello ${firstName} ${middleName} ${lastName}! It's nice to meet you!`);

function addAndSubtract(a,b) {
      return [a+b, a-b];
}
const array = addAndSubtract(4,2);
console.log(array);

const [sum, difference] = addAndSubtract(4,2);
console.log(sum);
console.log(difference);

const letters = ['A', 'B', 'C', 'D', 'E'];
const [a, ...rest] = letters; //... is called SPREAD OPERATOR
// console.log(a);
console.log(rest);
// ===========================================================

// DESTRUCTURE OBJECTS
console.warn('Object Destructuring:');
// allows to unpack properties of an object into distinct variables
// based on the name of the key

const person = {
      givenName: 'Jane',
      maidenName: 'Dela',
      familyName: 'Cruz'
};

// before
console.log(person.givenName);
console.log(person.maidenName);
console.log(person.familyName);
console.log(`Hello, name is ${person.givenName} ${person.maidenName} ${person.familyName}!`);

// object destructuring
const {givenName, maidenName, familyName} = person;
console.log(givenName);
console.log(maidenName);
console.log(familyName);
console.log(`Hello, name is ${person.givenName} ${person.maidenName} ${person.familyName}!`);

// function printUser(user) {
//       console.log(user);
// }
function printUser({givenName: nickName, familyName}) {
      console.log(`I am ${nickName} ${familyName}`);
}
printUser(person);


// =============================================================

// ARROW FUNCTIONS
console.warn('Arrow Functions');
// Syntax: 
      // const/let variabelName = (parameters) => {statements};

// before
const hello = function () {
      console.log('Hello World');
}

// using arrow function
const helloAgain = () => {
      console.log('Hello World');
}
hello();
helloAgain();
// ========================

// before
function printFullName(firstName, middleName, lastName) {
      console.log(firstName + ' ' + middleName + '. ' + lastName);
}
printFullName('John', 'D', 'Smith');

// using arrow function
const printFullName2 = (firstName, middleName, lastName) => {
      console.log(`${firstName} ${middleName}. ${lastName}`);
}
printFullName2('John', 'D', 'Smith');
// =====================

const numbers = [1,2,3,4,5];
// before
let numberMap = numbers.map(function(number) {
      return number * number;
})
console.log(numberMap);

// using arrow function
let numberMap2 = numbers.map((number) => {
      return number * number;
})
console.log(numberMap2);
// =======================

// Arrow Functions using loops
const students = ['Joy', 'Jade', 'Judy'];

// before
students.forEach(function(student) {
      console.log(`${student} is a student.`)
})

// using arrow function
students.forEach((student) => {
      console.log(`${student} is a student.`)
})
// =============================

// => Impicit Return Statement
console.warn('Arrow Functions: Implicit Return');
const add = (x,y) => x + y;
let total = add(1, 2);
console.log(total);
// =========================================================

// DEFAULT FUNCTION ARGUMENT VALUE
console.warn('Default Function Argument Value');

const greet = (name = 'User') => {
      return `Good Morning, ${name}`;
}
console.log(greet('Jake'));
console.log(greet());

const person2 = {
      name2: 'Jill',
      age: 27,
      email: 'jill@mail.com',
      address: {
            city: 'Some city',
            street: 'Some street'
      }
}
const {name2, age, email = null} = person2
console.log(name2);
console.log(age);
console.log(email);
// ====================================================

// CLASS-BASED OBJECT BLUEPRINT
console.warn('Class-Based Object Blueprints')
// allows creation of objects using classes as blueprints
// the constructor is a special method of a class for creating an object for that class
// syntax:
      // class className {
      //       constructor(propertyA, propertyB) {
      //             this.propertyA = propertyA
      //             this.propertyB = propertyB
      //       }
      // }
class Car {
      constructor(brand, name, year) {
            this.brand = brand;
            this.name = name;
            this.year = year;
      }
}
// creating a new instance of Car object
const myCar = new Car();
console.log(myCar);

// values of properties may be assigned after creation of an object
myCar.brand = 'Ford';
myCar.name = 'Ranger Raptor';
myCar.year = 2021;
console.log(myCar);

// creating new instance of car with initalized values
const myNewCar = new Car('Toyota', 'Vios', 2021);
console.log(myNewCar);